package tests;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import pageObjects.MainPage;
import supportClasses.WebCreator;

/**
 * This test consists in testing the error warning in case of an existent user tries to create another account.
 * @author Erick Cabral
 */
public class ExistentUserJoinTest {

	WebDriver browser;

	public ExistentUserJoinTest() {
	}

	@Before
	public void setUp() {
		WebCreator driver = new WebCreator("chrome", 5);
		this.browser = driver.getBrowser();
	}
	
	@Test
	public void existentUserJoin() throws Exception {

		boolean warningDiv = new MainPage(browser)
			.clickJoin()
			.inputUserData()
			.selectCountry()
			.inputDetailedData("873989435")
			.clickAcceptTerms()
			.clickCreateAccount()
			.waringDisplayed();

		assertTrue(warningDiv);
	}

	@After
	public void tearDown() {
		//this.browser.quit();
	}

}
