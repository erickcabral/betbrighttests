package tests;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import pageObjects.MainPage;
import supportClasses.WebCreator;

/**
 * This test consists in getting the Pin input window when a new user creates an account (all user information are correct).
 * 
 * @author Erick Cabral
 */
public class NewUserJoinTest {

	WebDriver browser;

	public NewUserJoinTest() {
	}

	@Before
	public void setUp() {
		WebCreator driver = new WebCreator("chrome", 5);
		this.browser = driver.getBrowser();
	}

	@Test
	public void newUserJoin() throws Exception {

		boolean pinWindowIsShown = new MainPage(browser)
			.clickJoin()
			.inputUserData()
			.selectCountry()
			.inputDetailedData("838486279")
			.clickAcceptTerms()
			.clickCreateAccount()
			.pinWindowDisplayed();

		assertTrue(pinWindowIsShown);
	}
	
	@After
	public void tearDown() {
		//this.browser.quit();
	}

}
