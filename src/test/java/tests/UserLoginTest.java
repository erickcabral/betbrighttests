/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests;

import org.junit.After;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import pageObjects.MainPage;
import supportClasses.WebCreator;

/**
 * This test consists in testing the error warning in case of an existent user tries to create another account.
 *
 * @author Erick Cabral
 */
public class UserLoginTest {

	private WebDriver browser;

	public UserLoginTest() {
	}

	@Before
	public void setUp() {
		WebCreator driver = new WebCreator("chrome", 5);
		this.browser = driver.getBrowser();
	}

	@Test
	public void userLoginTest() {
		boolean userIsLogged = new MainPage(this.browser)
			.clickLogin()
			.enterUserData()
			.clickLoginButton()
			.userLogin()
			.clickNoThanks()
			.assertWebElement("li", "id", "login_box");

		assertTrue(userIsLogged);
	}

	@After
	public void tearDown() {
	}

}
